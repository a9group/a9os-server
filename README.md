#A9OS - Choplifter

This repo contains a modified version of the Buildroot project's embedded Linux distribution targeting AMD64 and ARM64 platforms.

Self-build with system-level replication is the current goal, leveraging rsyphon and ipxe to provide automated builds with a continuous integration and delivery path built into the platform and workflow.



##Features
* Linux 4.7.2 Kernel
* Docker Engine, Containerd
* Basic GNU Userland; bootutils, coreutils, fsck, losetup, kill, pivot_root, setterm, wall, write, etc.
* Wireless Firmware, wpa_supplicant, other WiFi utils - rfkill (for development laptop use)
* Realtime Kernel patch
* bash
* Performance analysis tools; ntop, htop, iotop, perf, powertop, sysdig, etc.
* Tools, utilities to support self-build; wget, make, git, grep, flex, others (*in development*)
* Built with: GCC 5, glibc, Buildroot toolchain
* UEFI boot utilities; boot manager, efivar
* vim
* tmux
* xinetd
* macchanger
* ngrep
* dhcp server/client, dhcpdump
* collectd
* readline
* ncurses, dialog
* openssl
* sqlite
* More (see .config or server_x86_64.sysvinit.config for full scope)

##Details

Root password: **Burgert1m3**

Bootable kernel contains compressed root filesystem in initrd

ISO, raw disk images are also generated (dated with postbuild.sh)

Local time - "Etc/UTC"

#!/bin/bash
#
# A9OS setup script
#
#########################

if [ ! -f .config ]
then
	echo "Copying server.x86_64.sysvinit.config as default - '.config'.."
	cp server.x86_64.sysvinit.config .config
else
	echo "Using local .config"
fi

#!/bin/bash
# postbuild.sh
# 
# Runs after packaging
#
# wm@a9group.net
# Sat Mar  7 22:12:05 MST 2015
#######################

move_iso(){
echo "-> moving and renaming ISO image: output/images/a9os-server.$(date +%Y%m%d).iso"
mv output/images/rootfs.iso9660 output/images/a9os-server.$(date +%Y%m%d).iso
}

echo
echo "-------------------------------"
echo "postbuild.sh"
echo "-------------------------------"
echo
move_iso

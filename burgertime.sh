#!/bin/bash

# Based on SEE YOU SPACE COWBOY by DANIEL REHN (danielrehn.com)

# Cosmic color sequence

ESC_SEQ="\x1b[38;5;"
COL_01=$ESC_SEQ"160;01m"
COL_02=$ESC_SEQ"196;01m"
COL_03=$ESC_SEQ"202;01m"
COL_04=$ESC_SEQ"208;01m"
COL_05=$ESC_SEQ"214;01m"
COL_06=$ESC_SEQ"220;01m"
COL_07=$ESC_SEQ"226;01m"
COL_08=$ESC_SEQ"190;01m"
COL_09=$ESC_SEQ"154;01m"
COL_10=$ESC_SEQ"118;01m"
COL_11=$ESC_SEQ"046;01m"
COL_12=$ESC_SEQ"047;01m"
COL_13=$ESC_SEQ"048;01m"
COL_14=$ESC_SEQ"049;01m"
COL_15=$ESC_SEQ"051;01m"
COL_16=$ESC_SEQ"039;01m"
COL_17=$ESC_SEQ"027;01m"
COL_18=$ESC_SEQ"021;01m"
COL_19=$ESC_SEQ"021;01m"
COL_20=$ESC_SEQ"057;01m"
COL_21=$ESC_SEQ"093;01m"
RESET="\033[m"

# Timeless message
printf "\n"
printf "$COL_01 :::::::.   ...    ::::::::::..    .,-:::::/ .,:::::: :::::::.. :::::::::::::::.        :  .,::::::  \n"
printf "$COL_02  ;;;'';;'  ;;     ;;;;;;;``;;;; ,;;-'````'  ;;;;'''' ;;;;``;;;;;;;;;;;;'''';;;;;,.    ;;; ;;;;''''  \n"
printf "$COL_03  [[[__[[\.[['     [[[ [[[,/[[[' [[[   [[[[[[/[[cccc   [[[,/[[['     [[     [[[[[[[, ,[[[[, [[cccc   \n"
printf "$COL_04  \$\$\"\"\"\"Y\$\$\$\$      \$\$\$ \$\$\$\$\$\$c   \"\$\$c.    \"\$\$ \$\$\"\"\"\"   \$\$\$\$\$\$c       \$\$     \$\$\$\$\$\$\$\$\$\$\$\"\$\$\$ \$\$\"\"\"\"  \n" 
printf "$COL_05 _88o,,od8P88    .d888 888b \"88bo,\`Y8bo,,,o88o888oo,__ 888b \"88bo,   88,    888888 Y88\" 888o888oo,__ \n"
printf "$COL_06 \"\"YUMMMP\"  \"YmmMMMM\"\" MMMM   \"W\"   \`\'YMUP\"YMM\"\"\"\"YUMMMMMMM   \"W\"    MMM    MMMMMM  M'  \"MMM\"\"\"\"YUMMM\n"

printf "\n"
printf "$RESET" # Reset colors to "normal"
